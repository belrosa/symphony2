<?php

namespace EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{

    public function indexAction()
    {
    	$em = $this->getDoctrine()->getManager();

    	// Retrieve recommended products
		$products_repository = $em->getRepository('EcommerceBundle:Products');
		$products = $products_repository->getRecommendedProducts();

        return $this->render('EcommerceBundle:Default:index.html.smarty', [
        	'welcome_message' => 'test welcome message',
        	'recommended_products' => $products
        ]);
    }


    public function listAction($category, $page)
    {
    	// Set up
    	$category = urldecode($category);
    	$em = $this->getDoctrine()->getManager();
		$products_repository = $em->getRepository('EcommerceBundle:Products');
		$category_repository = $em->getRepository('EcommerceBundle:Categories');

    	// Get Products
		$category = $category_repository->findOneByName($category);
		$products = $products_repository->getPage($category->getId(), $page);

		// Paging Logic
		$totalPages   = ceil(count($products) / 10);
		$previousPage = max($page - 1, 1);
		$nextPage     = min($page + 1, $totalPages);

		// Breadcrumbs
		$breadcrumb = $category_repository->getBreadcrumb($category);

    	return $this->render('EcommerceBundle:Default:list.html.smarty', [
    		'products'      => $products,
    		'category'      => $category,
    		'breadcrumb'    => $breadcrumb,
    		'totalProducts' => count($products),
    		'currentPage'   => $page,
    		'previousPage'  => $previousPage,
    		'nextPage'      => $nextPage,
    		'totalPages'    => $totalPages
    	]);
    }


    public function showMenuAction()
    {
		// Build Menu
		$em = $this->getDoctrine()->getManager();
		$category_repository = $em->getRepository('EcommerceBundle:Categories');
		$categories = $category_repository->getMenu();
    	return $this->render('EcommerceBundle:partials:menu.html.smarty', [
    		'menu' => $categories
    	]);
    }

    public function productAction($sku)
    {

		$em = $this->getDoctrine()->getManager();
		$products_repository = $em->getRepository('EcommerceBundle:Products');
		$category_repository = $em->getRepository('EcommerceBundle:Categories');

		$product = $products_repository->findOneBySku($sku);

		if ($product)
		{
			$product_breadcrumb = $products_repository->getBreadcrumb($product);
			$product_breadcrumb[0]->url = $this->generateUrl('ecommerce_product', array('sku' => $product->getSku()));
			$category_breadcrumb = $category_repository->getBreadcrumb($product->getCategory());
			foreach ($category_breadcrumb as &$bc)
			{
				$bc->url = $this->generateUrl('ecommerce_plist', array('category' => $bc->getMenuSlug()));
			}

			$breadcrumb = array_merge($category_breadcrumb, $product_breadcrumb);


	    	return $this->render('EcommerceBundle:Default:product.html.smarty', [
	    		'product'    => $product,
	    		'breadcrumb' => $breadcrumb
	    	]);
		}
		else
		{
			// redirect to 404
		}
    }
}

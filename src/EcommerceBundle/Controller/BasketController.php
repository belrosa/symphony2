<?php

namespace EcommerceBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Request;

class BasketController extends Controller
{

    public function indexAction()
    {
		$session = new Session();

		$em = $this->getDoctrine()->getManager();
		$basket_repository = $em->getRepository('EcommerceBundle:Basket');
        $products_repository = $em->getRepository('EcommerceBundle:Products');
		$basket = $basket_repository->find($session->getId());

        foreach ($basket->getProducts() as &$item)
        {
            $product = $products_repository->findOneById($item->getProductId());
            $item->setProduct($product);
        }

    	return $this->render('EcommerceBundle:Default:basket.html.smarty', [
    		'basket' => $basket
    	]);
    }

    public function addAction($sku, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $products_repository = $em->getRepository('EcommerceBundle:Products');
        $basket_repository = $em->getRepository('EcommerceBundle:Basket');

        $product = $products_repository->findOneBySku($sku);

        if ($product)
        {
            $basket = $basket_repository->addProduct($product);

            $em->persist($basket);
            $em->flush();
        }

        if ($request->isXmlHttpRequest())
        {
            // Ajax
        }
        else
        {
            return $this->redirect($this->generateUrl('ecommerce_basket'));
        }
    }

    public function showBasketAction()
    {
		$session = new Session();

		$em = $this->getDoctrine()->getManager();
		$basket_repository = $em->getRepository('EcommerceBundle:Basket');
		$basket = $basket_repository->find($session->getId());

    	return $this->render('EcommerceBundle:partials:basket.html.smarty', [
    		'basket' => $basket,
            'total' => $basket_repository->getTotal($basket)
    	]);
    }
}

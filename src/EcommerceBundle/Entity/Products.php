<?php

namespace EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Products
 *
 * @ORM\Table(name="products")
 * @ORM\Entity(repositoryClass="EcommerceBundle\Repository\ProductsRepository")
 */
class Products
{
    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=50, nullable=false)
     */
    private $sku;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=100, nullable=false)
     */
    private $name;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="float", precision=12, scale=0, nullable=false)
     */
    private $price;

    /**
     * @var string
     *
     * @ORM\Column(name="shortdescription", type="string", length=1000, nullable=false)
     */
    private $shortdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="longdescription", type="text", length=65535, nullable=false)
     */
    private $longdescription;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string", length=100, nullable=false)
     */
    private $image;

    /**
     * @var integer
     *
     * @ORM\Column(name="category_id", type="integer", nullable=true)
     */
    private $categoryId;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Categories", inversedBy="products", cascade={"persist"})
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @var float
     *
     * @ORM\Column(name="balance", type="float", precision=12, scale=0, nullable=true)
     */
    private $balance;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Productoptions", mappedBy="product")
     */
    private $productoptions;

    /**
     * Set sku
     *
     * @param string $sku
     * @return Products
     */
    public function setSku($sku)
    {
        $this->sku = $sku;

        return $this;
    }

    /**
     * Get sku
     *
     * @return string 
     */
    public function getSku()
    {
        return $this->sku;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Products
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set price
     *
     * @param float $price
     * @return Products
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set shortdescription
     *
     * @param string $shortdescription
     * @return Products
     */
    public function setShortdescription($shortdescription)
    {
        $this->shortdescription = $shortdescription;

        return $this;
    }

    /**
     * Get shortdescription
     *
     * @return string 
     */
    public function getShortdescription()
    {
        return $this->shortdescription;
    }

    /**
     * Set longdescription
     *
     * @param string $longdescription
     * @return Products
     */
    public function setLongdescription($longdescription)
    {
        $this->longdescription = $longdescription;

        return $this;
    }

    /**
     * Get longdescription
     *
     * @return string 
     */
    public function getLongdescription()
    {
        return $this->longdescription;
    }

    /**
     * Set image
     *
     * @param string $image
     * @return Products
     */
    public function setImage($image)
    {
        $this->image = $image;

        return $this;
    }

    /**
     * Get image
     *
     * @return string 
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * Set balance
     *
     * @param float $balance
     * @return Products
     */
    public function setBalance($balance)
    {
        $this->balance = $balance;

        return $this;
    }

    /**
     * Get balance
     *
     * @return float 
     */
    public function getBalance()
    {
        return $this->balance;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set category
     *
     * @param integer $category
     * @return Products
     */
    public function setCategory($category)
    {
        $this->category = $category;

        return $this;
    }

    /**
     * Get category
     *
     * @return integer 
     */
    public function getCategory()
    {
        return $this->category;
    }

    /**
     * Set categoryId
     *
     * @param integer $categoryId
     * @return Products
     */
    public function setCategoryId($categoryId)
    {
        $this->categoryId = $categoryId;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return integer 
     */
    public function getCategoryId()
    {
        return $this->categoryId;
    }

    /**
     * Get menu slug
     *
     * @return string
     */
    public function getMenuSlug()
    {
        $menuSlug = strtolower($this->getSku());
        return urlencode($menuSlug);
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->productoptions = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add productoptions
     *
     * @param \EcommerceBundle\Entity\Productoptions $productoptions
     * @return Products
     */
    public function addProductoption(\EcommerceBundle\Entity\Productoptions $productoptions)
    {
        $this->productoptions[] = $productoptions;

        return $this;
    }

    /**
     * Remove productoptions
     *
     * @param \EcommerceBundle\Entity\Productoptions $productoptions
     */
    public function removeProductoption(\EcommerceBundle\Entity\Productoptions $productoptions)
    {
        $this->productoptions->removeElement($productoptions);
    }

    /**
     * Get productoptions
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProductoptions()
    {
        return $this->productoptions;
    }
}

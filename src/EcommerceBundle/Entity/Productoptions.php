<?php

namespace EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Productoptions
 *
 * @ORM\Table(name="productoptions")
 * @ORM\Entity
 */
class Productoptions
{
    /**
     * @var integer
     *
     * @ORM\Column(name="product_id", type="integer", nullable=false)
     */
    private $productId;

    /**
     * @var integer
     *
     * @ORM\Column(name="option_id", type="integer", nullable=false)
     */
    private $optionId;

    /**
     * @var float
     *
     * @ORM\Column(name="OptionPriceIncrement", type="float", precision=10, scale=0, nullable=true)
     */
    private $optionpriceincrement;

    /**
     * @var integer
     *
     * @ORM\Column(name="OptionGroupID", type="integer", nullable=false)
     */
    private $optiongroupid;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Products", inversedBy="productoptions")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Options", inversedBy="options")
     * @ORM\JoinColumn(name="option_id", referencedColumnName="id")
     */
    private $options;

    /**
     * Set productId
     *
     * @param integer $productId
     * @return Productoptions
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set optionId
     *
     * @param integer $optionId
     * @return Productoptions
     */
    public function setOptionId($optionId)
    {
        $this->optionId = $optionId;

        return $this;
    }

    /**
     * Get optionId
     *
     * @return integer 
     */
    public function getOptionId()
    {
        return $this->optionId;
    }

    /**
     * Set optionpriceincrement
     *
     * @param float $optionpriceincrement
     * @return Productoptions
     */
    public function setOptionpriceincrement($optionpriceincrement)
    {
        $this->optionpriceincrement = $optionpriceincrement;

        return $this;
    }

    /**
     * Get optionpriceincrement
     *
     * @return float 
     */
    public function getOptionpriceincrement()
    {
        return $this->optionpriceincrement;
    }

    /**
     * Set optiongroupid
     *
     * @param integer $optiongroupid
     * @return Productoptions
     */
    public function setOptiongroupid($optiongroupid)
    {
        $this->optiongroupid = $optiongroupid;

        return $this;
    }

    /**
     * Get optiongroupid
     *
     * @return integer 
     */
    public function getOptiongroupid()
    {
        return $this->optiongroupid;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set product
     *
     * @param \EcommerceBundle\Entity\Products $product
     * @return Productoptions
     */
    public function setProduct(\EcommerceBundle\Entity\Products $product = null)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return \EcommerceBundle\Entity\Products 
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * Set options
     *
     * @param \EcommerceBundle\Entity\Optiongroups $options
     * @return Productoptions
     */
    public function setOptions(\EcommerceBundle\Entity\Optiongroups $options = null)
    {
        $this->options = $options;

        return $this;
    }

    /**
     * Get options
     *
     * @return \EcommerceBundle\Entity\Optiongroups 
     */
    public function getOptions()
    {
        return $this->options;
    }
}

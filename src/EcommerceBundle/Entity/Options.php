<?php

namespace EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Options
 *
 * @ORM\Table(name="options")
 * @ORM\Entity
 */
class Options
{
    /**
     * @var integer
     *
     * @ORM\Column(name="optiongroup_id", type="integer", nullable=true)
     */
    private $optiongroupId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=50, nullable=true)
     */
    private $name;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Productoptions", mappedBy="optionId")
     */
    private $options;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Optiongroups", inversedBy="optiongroup")
     * @ORM\JoinColumn(name="optiongroup_id", referencedColumnName="id")
     */
    private $optiongroup;

    /**
     * Set optiongroupId
     *
     * @param integer $optiongroupId
     * @return Options
     */
    public function setOptiongroupId($optiongroupId)
    {
        $this->optiongroupId = $optiongroupId;

        return $this;
    }

    /**
     * Get optiongroupId
     *
     * @return integer 
     */
    public function getOptiongroupId()
    {
        return $this->optiongroupId;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Options
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->options = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add options
     *
     * @param \EcommerceBundle\Entity\Productoptions $options
     * @return Options
     */
    public function addOption(\EcommerceBundle\Entity\Productoptions $options)
    {
        $this->options[] = $options;

        return $this;
    }

    /**
     * Remove options
     *
     * @param \EcommerceBundle\Entity\Productoptions $options
     */
    public function removeOption(\EcommerceBundle\Entity\Productoptions $options)
    {
        $this->options->removeElement($options);
    }

    /**
     * Get options
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Set optiongroup
     *
     * @param \EcommerceBundle\Entity\Optiongroups $optiongroup
     * @return Options
     */
    public function setOptiongroup(\EcommerceBundle\Entity\Optiongroups $optiongroup = null)
    {
        $this->optiongroup = $optiongroup;

        return $this;
    }

    /**
     * Get optiongroup
     *
     * @return \EcommerceBundle\Entity\Optiongroups 
     */
    public function getOptiongroup()
    {
        return $this->optiongroup;
    }
}

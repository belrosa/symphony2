<?php

namespace EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Basketproducts
 *
 * @ORM\Table(name="basketproducts")
 * @ORM\Entity
 */
class Basketproducts
{
    /**
     * @var string
     *
     * @ORM\Column(name="basket_id", type="string", length=128, nullable=true)
     */
    private $basketId;

    /**
     * @var integer
     *
     * @ORM\Column(name="product_id", type="integer", nullable=true)
     */
    private $productId;

    /**
     * @var integer
     *
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\ManyToOne(targetEntity="Basket", inversedBy="products", cascade={"all"})
     * @ORM\JoinColumn(name="basket_id", referencedColumnName="id")
     */
    private $basket;

    private $product;

    public function __construct()
    {
        $this->quantity = 0;
    }

    /**
     * Set basketId
     *
     * @param string $basketId
     * @return Basketproducts
     */
    public function setBasketId($basketId)
    {
        $this->basketId = $basketId;

        return $this;
    }

    /**
     * Get basketId
     *
     * @return string 
     */
    public function getBasketId()
    {
        return $this->basketId;
    }

    /**
     * Set productId
     *
     * @param integer $productId
     * @return Basketproducts
     */
    public function setProductId($productId)
    {
        $this->productId = $productId;

        return $this;
    }

    /**
     * Get productId
     *
     * @return integer 
     */
    public function getProductId()
    {
        return $this->productId;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Basketproducts
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set basket
     *
     * @param \EcommerceBundle\Entity\Basket $basket
     * @return Basketproducts
     */
    public function setBasket(\EcommerceBundle\Entity\Basket $basket = null)
    {
        $this->basket = $basket;

        return $this;
    }

    /**
     * Get basket
     *
     * @return \EcommerceBundle\Entity\Basket 
     */
    public function getBasket()
    {
        return $this->basket;
    }

    public function setProduct(\EcommerceBundle\Entity\Products $product)
    {
        $this->product = $product;

        return $this;
    }

    public function getProduct()
    {
        return $this->product;
    }
}

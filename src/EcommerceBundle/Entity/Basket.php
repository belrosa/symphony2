<?php

namespace EcommerceBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Basket
 *
 * @ORM\Table(name="basket")
 * @ORM\Entity(repositoryClass="EcommerceBundle\Repository\BasketRepository")
 */
class Basket
{
    /**
     * @var string
     *
     * @ORM\Column(name="id", type="string", length=128)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @ORM\OneToMany(targetEntity="Basketproducts", mappedBy="basket", cascade={"all"})
     */
    private $products;

    public function __construct()
    {
        $this->products = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return string 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Add products
     *
     * @param \EcommerceBundle\Entity\Basketproducts $products
     * @return Basket
     */
    public function addProduct(\EcommerceBundle\Entity\Basketproducts $products)
    {
        $this->products[] = $products;

        return $this;
    }

    /**
     * Remove products
     *
     * @param \EcommerceBundle\Entity\Basketproducts $products
     */
    public function removeProduct(\EcommerceBundle\Entity\Basketproducts $products)
    {
        $this->products->removeElement($products);
    }

    /**
     * Get products
     *
     * @return \Doctrine\Common\Collections\Collection 
     */
    public function getProducts()
    {
        return $this->products;
    }
}
